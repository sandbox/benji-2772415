<?php

/**
 * @file
 * Contains SearchServiceInterface.
 */

namespace Drupal\fac\SearchService;

/**
 * Interface defining the methods backend search services have to implement.
 *
 * Before a service object is used, the corresponding server's data will be read
 * from the database. (See AbstractSearchService for a list of fields.)
 */
interface SearchServiceInterface {

  /**
   * Constructs a service object.
   */
  public function __construct();

  /**
   * Form constructor for the service configuration form.
   *
   * @param array $form
   *   The stub of the service options part of the form.
   * @param array &$form_state
   *   The complete form state.
   * @param array &$values
   *   The sub-array of $form_state['values'] corresponding to this service.
   * @param array &$settings
   *   The current settings for this service.
   *
   * @return array
   *   A form array for setting service-specific options.
   */
  public function configurationForm(array $form, array &$form_state, array &$values, array &$settings);

  /**
   * Validation callback for the form returned by configurationForm().
   *
   * Use form_error() to flag errors on form elements.
   * Alterntaively, set '#validate' keys on elements in configurationForm().
   *
   * @param array $form
   *   The form returned by configurationForm().
   * @param array &$form_state
   *   The complete form state.
   * @param array &$values
   *   The sub-array of $form_state['values'] corresponding to this service.
   */
  public function configurationFormValidate(array $form, array &$form_state, array &$values);

  /**
   * Submit callback for the form returned by configurationForm().
   *
   * @param array $form
   *   The form returned by configurationForm().
   * @param array &$form_state
   *   The complete form state.
   * @param array &$values
   *   The sub-array of $form_state['values'] corresponding to this service.
   *
   * @return array
   *   An associative array of settings for this service.
   */
  public function configurationFormSubmit(array $form, array &$form_state, array &$values);

  /**
   * Performs a search call.
   *
   * @param string $key
   *   The key to use in the search query.
   * @param string $language
   *   The language to use in the search query.
   * @param array &$settings
   *   The settings as returned by configurationFormSubmit().
   *
   * @return array
   *   A keyed array with an array of items. Each item is a keyed array of
   *   entity_type and the entity_id.
   *
   * @code
   * // Example array structure:
   * array(
   *   'suggestions' => array(
   *     array(
   *       'entity_type' => 'node',
   *       'entity_id' => 1,
   *     ),
   *     array(
   *       'entity_type' => 'node',
   *       'entity_id' => 2,
   *     ),
   *   ),
   * );
   * @endcode
   */
  public function search($key, $language, array &$settings);

}
