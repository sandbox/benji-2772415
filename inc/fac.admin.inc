<?php

/**
 * @file
 * This file contains the settings forms of the Fast Autocomplete module.
 */

/**
 * Fast AutoComplete settings form.
 */
function fac_settings_form($form, &$form_state) {
  $form = array();

  // Get all available backend service options.
  $service_options = array();
  $service_infos = fac_get_service_info();
  foreach ($service_infos as $service_info) {
    $service_options[$service_info['class']] = $service_info['name'];
  }

  // Get the current per-form settings.
  $fac_forms = variable_get('fac_forms', array());

  $form['fac_forms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forms'),
    '#tree' => TRUE,
    '#description' => t('Configure the form(s) where Fast Autocomplete should be used.'),
    '#prefix' => '<div id="fac-forms-wrapper">',
    '#suffix' => '</div>',
  );

  if (empty($form_state['num_forms'])) {
    $form_state['num_forms'] = max(1, count($fac_forms));
  }
  $num_forms = $form_state['num_forms'];

  for ($form_index = 0; $form_index < $num_forms; $form_index++) {
    $form['fac_forms']["form_$form_index"] = array(
      '#type' => 'fieldset',
      '#title' => t('Form #@form', array('@form' => $form_index)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['fac_forms']["form_$form_index"]['id'] = array(
      '#type' => 'textfield',
      '#title' => t('Form id'),
      '#description' => t('Enter the id attribute of the form. This is not the same as the form_id. For example, a Views exposed form will have form_id %form_id, but its id attribute will be %attribute.', array('%form_id' => 'views_exposed_form', '%attribute' => 'views-exposed-form-VIEW-DISPLAY')),
      '#default_value' => $fac_forms["form_$form_index"]['id'],
    );

    $form['fac_forms']["form_$form_index"]['backend_service'] = array(
      '#type' => 'select',
      '#title' => t('The backend service to use'),
      '#description' => t('Select the backend service that is used by Fast Autocomplete. Note that if a service is dependent on another module (for example: the Search API backend service needs the Search API module to be enabled) it only shows up if that module is enabled.'),
      '#options' => $service_options,
      '#default_value' => $fac_forms["form_$form_index"]['backend_service'],
      '#required' => TRUE,
    );

    $form['fac_forms']["form_$form_index"]['input_selectors'] = array(
      '#type' => 'textarea',
      '#title' => t('Inputs to enable Fast Autocomplete on'),
      '#description' => t('Enter the jQuery selector(s) for text input elements to enable the Fast Autocomplete functionality on those elements. You can provide multiple selectors divided by commas.'),
      '#default_value' => $fac_forms["form_$form_index"]['input_selectors'],
      '#attributes' => array(
        'placeholder' => t('for example: input[name="search_block_form"], input[name="some_other_form"]'),
      ),
    );

  }
  $form['fac_forms']['form_' . ($num_forms - 1)]['#collapsed'] = FALSE;

  $form['fac_forms']['add_form'] = array(
    '#type' => 'submit',
    '#value' => t('Add another form'),
    '#submit' => array('fac_settings_form_add_one'),
    '#ajax' => array(
      'callback' => 'fac_settings_form_callback',
      'wrapper' => 'fac-forms-wrapper',
    ),
  );

  if ($num_forms > 1) {
    $form['fac_forms']['remove_form'] = array(
      '#type' => 'submit',
      '#value' => t('Remove last form'),
      '#submit' => array('fac_settings_form_remove_one'),
      '#ajax' => array(
        'callback' => 'fac_settings_form_callback',
        'wrapper' => 'fac-forms-wrapper',
      ),
    );
  }

  $form['behavior'] = array(
    '#type' => 'fieldset',
    '#title' => t('Behavior'),
  );

  $form['behavior']['fac_empty_result'] = array(
    '#type' => 'textarea',
    '#title' => t('Text to show when the search input gets focus and there is no search term in the input.'),
    '#description' => t('Enter the HTML to show when the search input gets focus and there is no search term in the input. Useful for "quick links" for instance.'),
    '#default_value' => variable_get('fac_empty_result', ''),
  );

  $form['behavior']['fac_all_results_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show a "view all results" link'),
    '#description' => t('Enable this option to show a "view all results" link below the suggestios,'),
    '#default_value' => variable_get('fac_all_results_link', TRUE),
  );

  $form['behavior']['fac_all_results_link_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum number of suggestions to show "view all results" link'),
    '#element_validate' => array('fac_validate_integer_positive_or_zero'),
    '#description' => t('Enter the minimum number of suggestions to have to show the "view all results" link. Enter "0" to always show the "view all results" link.'),
    '#default_value' => variable_get('fac_all_results_link_threshold', "0"),
    '#size' => 3,
    '#states' => array(
      'visible' => array(
        'input[name="fac_all_results_link"]' => array(
          'checked' => TRUE,
        ),
      ),
      'required' => array(
        'input[name="fac_all_results_link"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  // Get all available view modes and create the view modes option array.
  $entities_info = entity_get_info();
  $view_modes = array();
  foreach ($entities_info as $entity_info) {
    $view_modes = array_merge($view_modes, $entity_info['view modes']);
  }
  // Add two default view mode options (not returned by entity_get_info()).
  $view_mode_options = array(
    'default' => t('Default'),
    'full' => t('Full'),
  );
  foreach ($view_modes as $system_name => $view_mode) {
    $view_mode_options[$system_name] = $view_mode['label'];
  }

  $form['behavior']['fac_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Select which view mode to use for rendering the suggestions'),
    '#description' => t('The view mode that is used to render the suggestions the search terms result in. Note that when you select a view mode that is not configured for the suggestion entity, the default view mode is used.'),
    '#default_value' => variable_get('fac_view_mode', 'fac'),
    '#options' => $view_mode_options,
  );

  $form['behavior']['fac_key_min_length'] = array(
    '#type' => 'textfield',
    '#title' => t('The minimum key length to trigger Fast Autocomplete'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('Enter the minimum key length to trigger the Fast Autocomplete on an input field. The minimum value is 1.'),
    '#default_value' => variable_get('fac_key_min_length', 1),
    '#required' => TRUE,
    '#size' => 2,
  );

  $form['behavior']['fac_key_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('The maximum key length to trigger Fast Autocomplete'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('Enter the maximum key length to trigger the Fast Autocomplete on an input field. The minimum value is 1.'),
    '#default_value' => variable_get('fac_key_max_length', 5),
    '#required' => TRUE,
    '#size' => 2,
  );

  $form['behavior']['fac_breakpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Breakpoint'),
    '#description' => t('Enter a minimum width in pixels to disable the Fast Autocomplete behavior until this minimum width is reached. Insert 0 to always enable the Fast Autocomplete behavior.'),
    '#element_validate' => array('fac_validate_integer_positive_or_zero'),
    '#default_value' => variable_get('fac_breakpoint', 0),
  );

  $form['behavior']['fac_use_module_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include the css provided by the module in the output'),
    '#description' => t("Enable this option to include the css provided by the Fast Autocomplete module in the output. Don't forget to clear the Drupal cache after changing this option!"),
    '#default_value' => variable_get('fac_use_module_css', TRUE),
  );

  $form['json_files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Json files'),
  );

  $form['json_files']['fac_clean_up_files'] = array(
    '#type' => 'checkbox',
    '#title' => t('Periodically clean up json files'),
    '#description' => t('Enable cleaning up json files on cron to refresh the contents of the json files that contain the autocomplete suggestions.'),
    '#default_value' => variable_get('fac_clean_up_files', TRUE),
  );

  $form['json_files']['fac_files_expire_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Expire time'),
    '#description' => t('How old do the json files have to be to be considered expired? The value for this field should contain a relative string compared to now like "-1 month" or "-1 day"'),
    '#default_value' => variable_get('fac_files_expire_time', '-1 day'),
    '#size' => 20,
    '#states' => array(
      'visible' => array(
        'input[name="fac_clean_up_files"]' => array(
          'checked' => TRUE,
        ),
      ),
      'required' => array(
        'input[name="fac_clean_up_files"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  $form['json_files']['fac_bulk_generate_json_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bulk generate json files'),
    '#description' => t('Enable daily bulk generating and refreshing json files on cron instead of only generating json files on user request.'),
    '#default_value' => variable_get('fac_bulk_generate_json_enabled', FALSE),
  );

  $form['json_files']['fac_bulk_generate_json_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size'),
    '#description' => t('To what size of the search key should the json files be generated? CAUTION: setting a size larger than 2 rapidly increases the number of combinations to generate json files for!'),
    '#default_value' => variable_get('fac_bulk_generate_json_size', 2),
    '#size' => 3,
    '#states' => array(
      'visible' => array(
        'input[name="fac_bulk_generate_json_enabled"]' => array(
          'checked' => TRUE,
        ),
      ),
      'required' => array(
        'input[name="fac_bulk_generate_json_enabled"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  $form['json_files']['fac_files_delete_all'] = array(
    '#type' => 'button',
    '#value' => t('Delete all json files'),
    '#limit_validation_errors' => array(),
    '#name' => 'delete',
  );

  return system_settings_form($form);
}

/**
 * Submit handler for the "Add another form" button.
 */
function fac_settings_form_add_one($form, &$form_state) {
  ++$form_state['num_forms'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Remove last form" button.
 */
function fac_settings_form_remove_one($form, &$form_state) {
  if ($form_state['num_forms'] > 1) {
    --$form_state['num_forms'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Callback for the ajax-enabled buttons.
 */
function fac_settings_form_callback($form, &$form_state) {
  return $form['fac_forms'];
}

/**
 * Form element validation handler for integers that must be positive or zero.
 *
 * @see element_validate_integer_positive()
 */
function fac_validate_integer_positive_or_zero($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer or zero.', array('%name' => $element['#title'])));
  }
}

/**
 * Fast Autocomplete settings form validation callback.
 */
function fac_settings_form_validate(&$form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'delete') {
    drupal_goto('admin/config/search/fac/delete');
  }

  // Make sure there are no duplicate form ids, and save a mapping of ids to
  // indexes. Skip the values for the add/remove buttons.
  $ids = array();
  foreach ($form_state['values']['fac_forms'] as $form_index => $settings) {
    if (preg_match('/^form_(\d+)$/', $form_index, $matches)) {
      $ids[$matches[1]] = $settings['id'];
    }
  }
  $id_indexes = array_flip($ids);
  if (count($ids) == count($id_indexes)) {
    $form_state['values']['fac_id_indexes'] = $id_indexes;
  }
  else {
    $duplicates = array_diff_key($ids, array_unique($ids));
    foreach ($form_state['values']['fac_forms'] as $form_index => $settings) {
      if (in_array($settings['id'], $duplicates)) {
        form_set_error("fac_forms][$form_index][id", t('You have duplicate form ids.'));
      }
    }
  }

  if ($form_state['values']['fac_clean_up_files'] == 1 && empty($form_state['values']['fac_files_expire_time'])) {
    form_set_error('fac_files_expire_time', t('The field expire time is required!'));
  }

  if ($form_state['values']['fac_bulk_generate_json_enabled'] == 1 && empty($form_state['values']['fac_bulk_generate_json_size'])) {
    form_set_error('fac_bulk_generate_json_size', t('The field size is required!'));
  }
}

/**
 * Returns the backend service settings form.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 *
 * @return array
 *   The backend service settings form.
 */
function fac_backend_settings_form($form, &$form_state) {
  // Get the current per-form settings from the main form and this one.
  $fac_forms = variable_get('fac_forms', array());
  $fac_backends = variable_get('fac_backends', array());

  $form_state += array('values' => array());
  $form_state['values'] += array('fac_backends' => array());

  $form['fac_backends'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forms'),
    '#tree' => TRUE,
    '#description' => t('Configure the backend services for each form where Fast Autocomplete will be used.'),
  );

  for ($form_index = 0; $form_index < count($fac_forms); $form_index++) {
    if (empty($fac_forms["form_$form_index"]['backend_service'])) {
      drupal_set_message(t('You have not selected a backend service yet. Please select one below.'), 'warning');
      drupal_goto('admin/config/search/fac');
    }

    $args = array(
      '@form' => $form_index,
      '%id' => $fac_forms["form_$form_index"]['id'],
    );
    $subform = array(
      '#type' => 'fieldset',
      '#title' => t('Form #@form', $args),
      '#description' => t('Configuration for id %id', $args),
    );

    $backend_service = $fac_forms["form_$form_index"]['backend_service'];
    $service = new $backend_service();
    $form_state['values']['fac_backends'] += array("form_$form_index" => array());
    $values = &$form_state['values']['fac_backends']["form_$form_index"];
    $fac_backends += array("form_$form_index" => array());
    $settings = &$fac_backends["form_$form_index"];
    $subform = $service->configurationForm($subform, $form_state, $values, $settings);

    $form['fac_backends']["form_$form_index"] = $subform;

  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validates the backend service settings form submitted values.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 */
function fac_backend_settings_form_validate($form, &$form_state) {
  // Get the current per-form settings from the main form and this one.
  $fac_forms = variable_get('fac_forms', array());

  for ($form_index = 0; $form_index < count($fac_forms); $form_index++) {
    if (empty($fac_forms["form_$form_index"]['backend_service'])) {
      continue;
    }

    $backend_service = $fac_forms["form_$form_index"]['backend_service'];
    $service = new $backend_service();
    $values = &$form_state['values']['fac_backends']["form_$form_index"];
    $service->configurationFormValidate($form, $form_state, $values);
  }
}

/**
 * Submits the backend service settings form submitted values.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 */
function fac_backend_settings_form_submit($form, &$form_state) {
  // Get the current per-form settings from the main form.
  $fac_forms = variable_get('fac_forms', array());
  $settings = array();

  for ($form_index = 0; $form_index < count($fac_forms); $form_index++) {
    if (empty($fac_forms["form_$form_index"]['backend_service'])) {
      continue;
    }

    $backend_service = $fac_forms["form_$form_index"]['backend_service'];
    $service = new $backend_service();
    $values = &$form_state['values']['fac_backends']["form_$form_index"];
    $settings["form_$form_index"]
      = $service->configurationFormSubmit($form, $form_state, $values);
  }

  variable_set('fac_backends', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
  $link = l(
    t('delete all json files'),
    'admin/config/search/fac',
    array('fragment' => 'edit-fac-files-delete-all')
  );
  drupal_set_message(t(
    'Note that you might need to delete the cached JSON files. You can use the !link button at the bottom of the general settings form.',
    array('!link' => $link)
  ));
}

/**
 * Fast AutoComplete delete json files form.
 */
function fac_delete_form($form, $form_state) {
  $form = array();

  return confirm_form($form, t('Delete the Fast Autocomplete json files'), 'admin/config/search/fac', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Submit handler for the fac_delete_form.
 */
function fac_delete_form_submit($form, &$form_state) {
  fac_delete_json_files();

  $form_state['redirect'] = 'admin/config/search/fac';
}
