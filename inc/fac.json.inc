<?php

/**
 * @file
 * This file contains the json callback of the Fast Autocomplete module.
 */

/**
 * Fast AutoComplete json callback.
 *
 * @param string $language
 *   The language code, such as 'en'.
 * @param string $page
 *   The numeric index of the page as configured on admin/config/search/fac.
 * @param string $key
 *   The key to search for.
 *
 * @todo Use drupal_json_output(). See
 * http://stackoverflow.com/questions/3636463/drupal-create-a-page-that-outputs-json
 */
function fac_generate_json($language, $page, $key) {
  // Remove the .json part from the key.
  $key = str_replace('.json', '', $key);

  // Only continue if the requested key is shorter or equal to the configured
  // key length to search for.
  if (strlen($key) <= variable_get('fac_key_max_length', 5)) {
    // Get the result for the key which automatically saves a json file on the
    // file system.
    $json_response = fac_generate_json_for_key($language, $page, $key);

    // Return the json result.
    drupal_add_http_header('Content-Type', 'application/json');
    echo $json_response;
  }

  drupal_exit();
}
